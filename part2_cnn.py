import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Activation, Dense, Flatten, BatchNormalization, Conv2D, MaxPool2D
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.metrics import categorical_crossentropy
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.models import load_model
from sklearn.metrics import confusion_matrix
import itertools
import os
import shutil
import random
import glob
import matplotlib.pyplot as plt
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

def organise_data():
    # Organize data into train, valid, test dirs
    os.chdir('data/dogs-vs-cats')
    if os.path.isdir('train/dog') is False:
        os.makedirs('train/dog')
        os.makedirs('train/cat')
        os.makedirs('valid/dog')
        os.makedirs('valid/cat')
        os.makedirs('test/dog')
        os.makedirs('test/cat')
        
        # Training set
        for i in random.sample(glob.glob('cat*'), 500):
            shutil.move(i, 'train/cat')      
        for i in random.sample(glob.glob('dog*'), 500):
            shutil.move(i, 'train/dog')

        # Validation set
        for i in random.sample(glob.glob('cat*'), 100):
            shutil.move(i, 'valid/cat')        
        for i in random.sample(glob.glob('dog*'), 100):
            shutil.move(i, 'valid/dog')

        # Test data
        for i in random.sample(glob.glob('cat*'), 50):
            shutil.move(i, 'test/cat')      
        for i in random.sample(glob.glob('dog*'), 50):
            shutil.move(i, 'test/dog')

    os.chdir('../../')

def preprocess_imgs():
    train_path = 'data/dogs-vs-cats/train'
    valid_path = 'data/dogs-vs-cats/valid'
    test_path = 'data/dogs-vs-cats/test'

    # Preprocess images for VGG16 network
    # Resize to (224, 224)
    # Shuffle the data for training and validation sets but not for the test set (so we can match up test set to labels later)
    # The data is stored as DirectoryIterator objects
    train_batches = ImageDataGenerator(preprocessing_function=tf.keras.applications.vgg16.preprocess_input).flow_from_directory(
            directory=train_path, target_size=(224,224), classes=['cat', 'dog'], batch_size=10)
    valid_batches = ImageDataGenerator(preprocessing_function=tf.keras.applications.vgg16.preprocess_input).flow_from_directory(
            directory=valid_path, target_size=(224,224), classes=['cat', 'dog'], batch_size=10)
    test_batches = ImageDataGenerator(preprocessing_function=tf.keras.applications.vgg16.preprocess_input).flow_from_directory(
            directory=test_path, target_size=(224,224), classes=['cat', 'dog'], batch_size=10, shuffle=False)

    return train_batches, valid_batches, test_batches

def plotImages(images_arr):
    fig, axes = plt.subplots(1, 10, figsize=(20,20))
    axes = axes.flatten()
    for img, ax in zip( images_arr, axes):
        ax.imshow(img)
        ax.axis('off')
    plt.tight_layout()
    plt.show()

def build_cnn():
    # Build a sequential model
    # Note: input_shape only specified for the first hidden layer
    # padding='same' means zero-padding
    # Flatten() - flattens into 1D tensor
    # Last layer is Dense() - fully connected layer with 2 nodes (2 outputs, cat or dog) with softmax activation
    # NOTE: since in this case, we have 2 classes, we could use the sigmoid activation function instead
    # but softmax works for >= 2 classes
    model = Sequential([
    Conv2D(filters=32, kernel_size=(3, 3), activation='relu', padding='same', input_shape=(224, 224, 3)),
    MaxPool2D(pool_size=(2, 2), strides=2),
    Conv2D(filters=64, kernel_size=(3, 3), activation='relu', padding='same'),
    MaxPool2D(pool_size=(2, 2), strides=2),
    Flatten(),
    Dense(units=2, activation='softmax')
    ])
    model.summary()

    return model

def train_cnn(model, train_batches, valid_batches):
    # Configure model
    # NOTE: if the last layer of the network used a sigmoid activation function instead
    # (for 2 classes) then the loss function should be chosen as 'binary_crossentropy' instead
    model.compile(
        optimizer=Adam(learning_rate=0.0001),
        loss='categorical_crossentropy',
        metrics=['accuracy'])
    
    # Train model
    # epochs = 10 - train for 10 runs only
    # NOTE: we do not need to specify the y (labels) as the train_batches contains the labels already
    model.fit(x=train_batches, validation_data=valid_batches, epochs=10, verbose=2)

    return model

def save_model(model, path='models/cats-vs-dogs_model.h5'):
    # Save using model.save()
    # Saves everything - architecture, weights, training config, state of optimiser
    # Will not overwrite if file already exists
    if os.path.isfile(path) is False:
        model.save(path)

def predict(model, test_batches):
    # Make predictions
    predictions = model.predict(x=test_batches, verbose=0)

    # Get hard predictions
    rounded_predictions = np.round(predictions)

    return rounded_predictions

def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, cm[i, j],
            horizontalalignment="center",
            color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.show()

def get_confusion_matrix(test_batches, hard_predictions):
    # Note that hard_predictions is one-hot-encoded (e.g. [0, 1], [0,1], [1,0] ... etc)
    # So we use np.argmax to convert to e.g. [1, 1, 0, ... etc]
    # since test_batches.classes (the true labels) is in this format
    cm = confusion_matrix(y_true=test_batches.classes, y_pred=np.argmax(hard_predictions, axis=-1))

    # Plot confusion matrix
    # Check we have the right order
    assert test_batches.class_indices == {'cat': 0, 'dog': 1}
    cm_plot_labels = ['cat', 'dog']
    plot_confusion_matrix(cm=cm, classes=cm_plot_labels)

    return cm

def build_finetuned_vgg16(original_model):
    # Convert the keras api model to a Sequential() model
    model = Sequential()
    # NOTE: we leave out the very last layer hence the [:-1]
    for layer in original_model.layers[:-1]:
        model.add(layer)

    # Freeze the weights and biases in the model
    # as we don't want to retrain them
    for layer in model.layers:
        layer.trainable = False
    
    # Add the last layer (Dense, softmax, 2 outputs)
    model.add(Dense(units=2, activation='softmax'))

    model.summary()

    return model

def train_finetuned_model(model, train_batches, valid_batches):
    # Configure model
    model.compile(
        optimizer=Adam(learning_rate=0.0001),
        loss='categorical_crossentropy',
        metrics=['accuracy'])

    # Train model
    model.fit(x=train_batches, validation_data=valid_batches, epochs=5, verbose=2)

    return model

def main():
    """ PREPARING THE DATA
        ------------------ """
    # Organise the data from the dogs-vs-cats directory
    # organise_data()

    # Preprocess the images
    train_batches, valid_batches, test_batches = preprocess_imgs()
    
    # Grab a batch (10 images and their labels) from the training set and show them
    # imgs, labels = next(train_batches)
    # plotImages(imgs)
    # print(labels)


    """ BUILDING OUR OWN NETWORK
        ------------------------ """
    # Build and train network
    # model = build_cnn()
    # trained_model = train_cnn(model=model, train_batches=train_batches, valid_batches=valid_batches)

    # Save the trained_model
    # save_model(trained_model)

    # Load the saved model
    # trained_model = load_model('models/cats-vs-dogs_model.h5')
    # trained_model.summary()

    # Make predictions
    # rounded_predictions = predict(model=trained_model, test_batches=test_batches)
    # print(rounded_predictions)
    # cm = get_confusion_matrix(test_batches=test_batches, hard_predictions=rounded_predictions)


    """ FINE-TUNING VGG16 NETWORK
        ------------------------- """
    # # Download the original vgg16 model
    # vgg16_model = tf.keras.applications.vgg16.VGG16()
    # # Save it as a file
    # save_model(model=vgg16_model, path='models/vgg16_original.h5')

    # Load the model from file
    # vgg16_model = load_model('models/vgg16_original.h5')
    # vgg16_model.summary()
    # print('Type of vgg16 model', type(vgg16_model))

    # Build a fine-tuned version of the VGG16 model, then train it
    # modified_vgg16_model = build_finetuned_vgg16(vgg16_model)
    # trained_vgg16_model = train_finetuned_model(model=modified_vgg16_model, train_batches=train_batches, valid_batches=valid_batches)
    # Save it as a file
    # save_model(model=trained_vgg16_model, path='models/vgg16_finetuned_trained.h5')
    # Load trained model from file
    trained_vgg16_model = load_model('models/vgg16_finetuned_trained.h5')

    # Make predictions
    rounded_predictions = predict(model=trained_vgg16_model, test_batches=test_batches)
    print(rounded_predictions)
    cm = get_confusion_matrix(test_batches=test_batches, hard_predictions=rounded_predictions)

if __name__ == '__main__':
    main()