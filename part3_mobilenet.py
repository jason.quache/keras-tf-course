import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.layers import Dense, Activation
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.metrics import categorical_crossentropy
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.preprocessing import image
from tensorflow.keras.models import Model
from tensorflow.keras.models import load_model
from tensorflow.keras.applications import imagenet_utils
from sklearn.metrics import confusion_matrix
import itertools
import os
import shutil
import random
import matplotlib.pyplot as plt
import pprint


def save_model(model, path):
    # Save using model.save()
    # Saves everything - architecture, weights, training config, state of optimiser
    # Will not overwrite if file already exists
    if os.path.isfile(path) is False:
        model.save(path)

def prepare_image(file):
    img_path = 'data/MobileNet-samples/'
    img = image.load_img(img_path + file, target_size=(224, 224))

    # Convert (from PIL image format) to array
    img_array = image.img_to_array(img)

    # Expand dimensions to what Mobilenet expects
    img_array_expanded_dims = np.expand_dims(img_array, axis=0)

    # Preprocess image for Mobilenet and return it
    return tf.keras.applications.mobilenet.preprocess_input(img_array_expanded_dims)

def predict_img(model, filename):
    preprocessed_image = prepare_image(filename)
    predictions = model.predict(preprocessed_image)
    results = imagenet_utils.decode_predictions(predictions)
    pprint.pprint(results)

    return predictions

def organise_data(dir='data/Sign-Language-Digits-Dataset'):
    os.chdir(dir)
    # Create directories if not already present
    if os.path.isdir('train/0/') is False:
        os.mkdir('train')
        os.mkdir('valid')
        os.mkdir('test')
    
    for i in range(0, 10):
        # Move all images to train directory
        shutil.move(f'{i}', 'train')
        os.mkdir(f'valid/{i}')
        os.mkdir(f'test/{i}')

        # Take 30 random samples (for each class) and move it from train dir to valid dir
        valid_samples = random.sample(os.listdir(f'train/{i}'), 30)
        for j in valid_samples:
            shutil.move(f'train/{i}/{j}', f'valid/{i}')
        
        # Take 5 random samples (for each class) and move it from train dir to test dir
        test_samples = random.sample(os.listdir(f'train/{i}'), 5)
        for k in test_samples:
            shutil.move(f'train/{i}/{k}', f'test/{i}')
    
    os.chdir('../..')

def preprocess_images(dir='data/Sign-Language-Digits-Dataset'):
    train_path = '{}/train'.format(dir)
    valid_path = '{}/valid'.format(dir)
    test_path = '{}/test'.format(dir)

    train_batches = ImageDataGenerator(
        preprocessing_function=tf.keras.applications.mobilenet.preprocess_input).flow_from_directory(
            directory=train_path, target_size=(224,224), batch_size=10)
    valid_batches = ImageDataGenerator(
        preprocessing_function=tf.keras.applications.mobilenet.preprocess_input).flow_from_directory(
            directory=valid_path, target_size=(224,224), batch_size=10)
    # Do not shuffle the test data
    test_batches = ImageDataGenerator(
        preprocessing_function=tf.keras.applications.mobilenet.preprocess_input).flow_from_directory(
            directory=test_path, target_size=(224,224), batch_size=10, shuffle=False)

    return train_batches, valid_batches, test_batches

def build_finetuned_mobilenet(mobilenet_model):
    mobile = mobilenet_model
    # Remember the mobilenet model is a Functional model
    # Grab output from 6th to last layer
    x = mobile.layers[-6].output

    # Create our new output layer (10 outputs, softmax) and append it to the original mobilenet
    # with the last 5 layers chopped off
    # NOTE: this strange way of doing it is because of the Functional model
    output = Dense(units=10, activation='softmax')(x)
    model = Model(inputs=mobile.input, outputs=output)

    # Freeze the layers except the last 23 layers (nb: original model has 88 layers)
    for layer in model.layers[:-23]:
        layer.trainable = False
    
    model.summary()
    return model

def train_finetuned_model(model, train_batches, valid_batches):
    # Configure model
    model.compile(optimizer=Adam(learning_rate=0.0001), loss='categorical_crossentropy', metrics=['accuracy'])

    # Train model
    model.fit(x=train_batches, validation_data=valid_batches, epochs=30, verbose=2)

    return model

def predict(model, test_batches):
    # Make predictions
    predictions = model.predict(x=test_batches, verbose=0)

    # Get hard predictions
    rounded_predictions = np.round(predictions)

    return rounded_predictions

def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, cm[i, j],
            horizontalalignment="center",
            color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.show()

def get_confusion_matrix(test_batches, hard_predictions):
    # Note that hard_predictions is one-hot-encoded (e.g. [0, 1], [0,1], [1,0] ... etc)
    # So we use np.argmax to convert to e.g. [1, 1, 0, ... etc]
    # since test_batches.classes (the true labels) is in this format
    cm = confusion_matrix(y_true=test_batches.classes, y_pred=np.argmax(hard_predictions, axis=-1))

    # Plot confusion matrix
    cm_plot_labels = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
    plot_confusion_matrix(cm=cm, classes=cm_plot_labels)

    return cm

def main():
    # Download MobileNet
    # mobile = tf.keras.applications.mobilenet.MobileNet()
    # # Save it as a file
    # save_model(model=mobile, path='models/mobilenet_original.h5')

    # Load the model from file
    # mobile = load_model('models/mobilenet_original.h5')
    # mobile.summary()

    # Predict for a sample image
    # predict_img(model=mobile, filename='2.jpg')

    # Organise sign language data
    # organise_data()

    # Preprocess the sign language data
    train_batches, valid_batches, test_batches = preprocess_images()

    # Build fine-tuned model and train it
    # finetuned_mobile = build_finetuned_mobilenet(mobile)
    # trained_mobile = train_finetuned_model(model=finetuned_mobile, train_batches=train_batches, valid_batches=valid_batches)
    # Save it as a file
    # save_model(model=trained_mobile, path='models/mobilenet_signdigits_trained.h5')
    # Load the model from file
    trained_mobile = load_model('models/mobilenet_signdigits_trained.h5')
    trained_mobile.summary()

    # Make predictions and plot confusion matrix
    hard_predictions = predict(model=trained_mobile, test_batches=test_batches)
    get_confusion_matrix(test_batches=test_batches, hard_predictions=hard_predictions)


if __name__ == '__main__':
    main()