import numpy as np
from random import randint
from sklearn.utils import shuffle
from sklearn.preprocessing import MinMaxScaler
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Activation, Dense
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.metrics import categorical_crossentropy
from sklearn.metrics import confusion_matrix
import itertools
import matplotlib.pyplot as plt
import os.path
import json
from tensorflow.keras.models import model_from_json
from tensorflow.keras.models import load_model

def generate_data():
    train_labels = []
    train_samples = []

    # Generate some data
    for i in range(50):
        # The ~5% of younger individuals who did experience side effects
        random_younger = randint(13, 64)
        train_samples.append(random_younger)
        train_labels.append(1) # i.e. did experience side effects

        # The ~5% of older individuals who did not experience side effects
        random_older = randint(65, 100)
        train_samples.append(random_older)
        train_labels.append(0) # i.e. didn't experience side effects

    for i in range(1000):
        # The ~95% of younger individuals who did not experience side effects
        random_younger = randint(13, 64)
        train_samples.append(random_younger)
        train_labels.append(0)

        # The ~95% of older individuals who did experience side effects
        random_older = randint(65, 100)
        train_samples.append(random_older)
        train_labels.append(1)

    # print("training samples", train_samples)
    # print("training labels", train_labels)

    # Convert to numpy arrays
    train_labels = np.array(train_labels)
    train_samples = np.array(train_samples)
    train_labels, train_samples = shuffle(train_labels, train_samples)

    # Rescale the training data to be between 0 and 1 (normalise)
    scaler = MinMaxScaler(feature_range=(0,1))
    scaled_train_samples = scaler.fit_transform(train_samples.reshape(-1,1))

    # for i in scaled_train_samples:
    #     print(i)
    print("shape of data", scaled_train_samples.shape)
    return train_labels, scaled_train_samples

def create_model():
    # Create sequential neural network
    # Use dense layers (i.e. fully connected layer)
    # units = nodes in layer
    # input_shape = shape of input data (e.g. 1D vector)
    # activation = activation function
    model = Sequential([
        Dense(units=16, input_shape=(1,), activation='relu'),
        Dense(units=32, activation='relu'),
        Dense(units=2, activation='softmax')
    ])

    # Visualise layer
    model.summary()

    return model

def train_network(model, labels, samples):
    ## Compile model (configure it for training) ##
    model.compile(
        optimizer=Adam(learning_rate=0.0001),
        loss='sparse_categorical_crossentropy', 
        metrics=['accuracy'])
    
    ## Train model ##
    # x = training samples (input data)
    # y = training labels (target data)
    # validation_split = fraction of data to use as validation set
    # (validation set is not used for training, used to see how well model
    # generalising during training)
    # NOTE: validation set is split off BEFORE the shuffle occurs
    # hence it is important to pre-shuffle data before passing to fit function
    # despite setting shuffle=True for the fit function

    # batch_size = number of samples to process at a time
    # epochs = number of times to process the dataset
    # shuffle = shuffle the data in the dataset (true by default)
    # verbose = 2 (most verbose level) or 1 or 0
    model.fit(
        x=samples,
        y=labels,
        validation_split=0.1,
        batch_size=10,
        epochs=30,
        shuffle=True,
        verbose=2)
    
    return model

def create_test_set():
    test_labels =  []
    test_samples = []

    for i in range(10):
        # The 5% of younger individuals who did experience side effects
        random_younger = randint(13,64)
        test_samples.append(random_younger)
        test_labels.append(1)
        
        # The 5% of older individuals who did not experience side effects
        random_older = randint(65,100)
        test_samples.append(random_older)
        test_labels.append(0)

    for i in range(200):
        # The 95% of younger individuals who did not experience side effects
        random_younger = randint(13,64)
        test_samples.append(random_younger)
        test_labels.append(0)
        
        # The 95% of older individuals who did experience side effects
        random_older = randint(65,100)
        test_samples.append(random_older)
        test_labels.append(1)

    test_labels = np.array(test_labels)
    test_samples = np.array(test_samples)
    test_labels, test_samples = shuffle(test_labels, test_samples)

    scaler = MinMaxScaler(feature_range=(0,1))
    scaled_test_samples = scaler.fit_transform(test_samples.reshape(-1,1))

    return test_labels, scaled_test_samples

def predict(model, test_labels, test_samples):
    # Make predictions
    # Each prediction is of the form [Pr(y=0), Pr(y=1)]
    predictions = model.predict(
        x=test_samples,
        batch_size=10,
        verbose=0
    )
    print(predictions)

    # Get hard predictions (most probable y)
    rounded_predictions = np.argmax(predictions, axis=-1)
    print(rounded_predictions)

    # Calculate confusion matrix
    cm = confusion_matrix(y_true=test_labels, y_pred=rounded_predictions)
    
    # Plot confusion matrix
    cm_plot_labels = ['no_side_effects', 'had_side_effects']
    plot_confusion_matrix(
        cm=cm,
        classes=cm_plot_labels,
        title='Confusion Matrix'
    )
    
def plot_confusion_matrix(cm, classes,
                        normalize=False,
                        title='Confusion matrix',
                        cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, cm[i, j],
            horizontalalignment="center",
            color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.show()

def save_model(model, path='models/medical_trial_model.h5'):
    # Save using model.save()
    # Saves everything - architecture, weights, training config, state of optimiser
    # Will not overwrite if file already exists
    if os.path.isfile(path) is False:
        model.save(path)

def save_model_architecture(model, path='models/medical_trial_model_architecture.json'):
    # Save as JSON
    json_string = model.to_json()

    # Save to file
    with open(path, 'w') as json_file:
        json.dump(json_string, json_file)

def reconstruct_model_json(path_to_json='models/medical_trial_model_architecture.json'):
    with open(path_to_json, 'r') as json_file:
        json_string = json.load(json_file)

    model_architecture = model_from_json(json_string)
    return model_architecture

def save_model_weights(model, path='models/medical_trial_model_weights.h5'):
    # Will not overwrite if file already exists
    if os.path.isfile(path) is False:
            model.save_weights(path)

def main():
    # Generate data
    train_labels, scaled_train_samples = generate_data()

    # Create the model
    network = create_model()

    # Train the network
    trained_network = train_network(
        model=network,
        labels=train_labels,
        samples=scaled_train_samples)
    
    # Save model
    save_model(trained_network)
    save_model_architecture(trained_network)
    # model_architecture = reconstruct_model_json()
    # model_architecture.summary()
    save_model_weights(trained_network)

    # # Could create new model and load the previous saved weights into it
    # model2 = Sequential([
    # Dense(units=16, input_shape=(1,), activation='relu'),
    # Dense(units=32, activation='relu'),
    # Dense(units=2, activation='softmax')
    # ])
    # model2.load_weights('models/medical_trial_model_weights.h5')
    # model2.get_weights()

    # Generate some test data
    test_labels, scaled_test_samples = create_test_set()

    # Make predictions
    predict(
        model=trained_network,
        test_labels=test_labels,
        test_samples=scaled_test_samples
    )

if __name__ == '__main__':
    main()
